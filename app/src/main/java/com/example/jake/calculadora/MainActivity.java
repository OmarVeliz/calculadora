package com.example.jake.calculadora;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Button;

public class MainActivity extends ActionBarActivity {

    private Button btnRaiz1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    Double numero1, numero2, resultado;
    String operando;


    public void btnNumero1 (View view)
    {
        TextView textV = (TextView)findViewById(R.id.textView) ;
        textV.setText(textV.getText() + "1");
    }

    public void btnNumero2 (View view)
    {
        TextView textV = (TextView)findViewById(R.id.textView) ;
        textV.setText(textV.getText() + "2");
    }

    public void btnNumero3 (View view)
    {
        TextView textV = (TextView)findViewById(R.id.textView) ;
        textV.setText(textV.getText() + "3");
    }

    public void btnNumero4 (View view)
    {
        TextView textV = (TextView)findViewById(R.id.textView) ;
        textV.setText(textV.getText() + "4");
    }

    public void btnNumero5 (View view)
    {
        TextView textV = (TextView)findViewById(R.id.textView) ;
        textV.setText(textV.getText() + "5");
    }

    public void btnNumero6 (View view)
    {
        TextView textV = (TextView)findViewById(R.id.textView) ;
        textV.setText(textV.getText() + "6");
    }

    public void btnNumero7 (View view)
    {
        TextView textV = (TextView)findViewById(R.id.textView) ;
        textV.setText(textV.getText() + "7");
    }

    public void btnNumero8 (View view)
    {
        TextView textV = (TextView)findViewById(R.id.textView) ;
        textV.setText(textV.getText() + "8");
    }

    public void btnNumero9 (View view)
    {
        TextView textV = (TextView)findViewById(R.id.textView) ;
        textV.setText(textV.getText() + "9");
    }

    public void btnNumero0 (View view)
    {
        TextView textV = (TextView)findViewById(R.id.textView) ;
        textV.setText(textV.getText() + "0");
    }

    public void CapturaDeDato1(View view) {
        TextView captura =(TextView)findViewById(R.id.textView);
        numero1 = Double.parseDouble(captura.getText().toString());
        captura.setText("");
    }

    public void btnMas (View view)  {
        operando = "+";
        CapturaDeDato1(view);
    }

    public void btnMenos (View view){
        operando = "-";
        CapturaDeDato1(view);
    }

    public void btnDividir (View view)  {
        operando = "/";
        CapturaDeDato1(view);
    }

    public void btnMultiplicacion (View view) {
        operando = "*";
        CapturaDeDato1(view);
    }

    public void btnIgual (View view) {

        TextView igual = (TextView) findViewById(R.id.textView);
        numero2 = Double.parseDouble(igual.getText().toString());

        operando.toString();

        try {
            switch (operando) {
                case "+":
                    try {
                        resultado = numero1 + numero2;
                        igual.setText(resultado.toString());
                    }catch (Exception e) {
                        Context context = getApplicationContext();
                        CharSequence text = "Se ha encontrado un error";
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }
                    break;
                case "-":
                    try {
                        resultado = numero1 - numero2;
                        igual.setText(resultado.toString());
                    } catch (Exception e) {
                        Context context = getApplicationContext();
                        CharSequence text = "Se ha encontrado un error";
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }
                    break;
                case "/":
                    try {
                        resultado = numero1 / numero2;
                        igual.setText(resultado.toString());
                    } catch (Exception e) {
                        Context context = getApplicationContext();
                        CharSequence text = "No existe!";
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }
                    break;
                case "*":
                    try {
                        resultado = numero1 * numero2;
                        igual.setText(resultado.toString());
                    } catch (Exception e){
                        Context context = getApplicationContext();
                        CharSequence text = "No existe!";
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }
                    break;
                case "^":
                    try {
                        resultado = Math.pow(numero1, numero2);
                        igual.setText(resultado.toString());
                    } catch (Exception e) {
                        Context context = getApplicationContext();
                        CharSequence text = "No valida esta operacion";
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }
                    break;
            }
        } catch (Exception e) {
            Context context = getApplicationContext();
            CharSequence text = "Operacion invalida";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }

    public void btnBorrar (View view) {
        numero1 = 0.0;
        numero2 = 0.0;
        TextView limpiar = (TextView) findViewById(R.id.textView);
        limpiar.setText("");
    }

    //Button botonRaiz = (Button)findViewById(R.id.button17);
    public void btnRaiz(View view) {
        try {
            TextView igual1 = (TextView) findViewById(R.id.textView);
            String contenedor = igual1.getText().toString();
            numero1 = Double.parseDouble(contenedor);
            resultado = Math.sqrt(numero1);
            contenedor = String.valueOf(resultado);
            igual1.setText(contenedor);
        } catch (Exception e){
            Context context = getApplicationContext();
            CharSequence text = "Debe ingresar el numero luego el signo";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }

    public void btnExponente(View view){
        operando = "^";
        CapturaDeDato1(view);
    }

    public void btnLog10 (View view) {
        try {
            TextView logNumero = (TextView) findViewById(R.id.textView);
            String contLog = logNumero.getText().toString();
            numero1 = Double.parseDouble(contLog);
            resultado = Math.log10(numero1);
            logNumero.setText(resultado.toString());
        } catch (Exception e) {
            Context context = getApplicationContext();
            CharSequence text = "Seleccione el numero luego este boton";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }
}
